# Remote Co-Verification

An effective Remote Co-Verification (ReCoVer) library of hardware and software
co-designs

## Virtual environment

Preparing a virtual environment for the project development:

```
. ./env-setup.sh
```
